import numpy as np
from utility.logging_utility import get_normal_logger
from utility.waveform_processing import WaveformProcessing


sampling_rate = 1.4e9  # サンプリングレート
time = 1.2e-6  # 波形の時間
length = round(sampling_rate * time)  # 配列の大きさ
time_array = np.linspace(
    - time / 2.0, time / 2.0, length, endpoint=False
)  # 時間の配列
waveform = (
    np.sin(2.0 * np.pi * 1e8 * time_array)
    + np.sin(2.0 * np.pi * 2e8 * time_array)
    + 0.25
)  # sin wave
# waveform = np.sin(2. * np.pi * 1e8 * time_array) * np.exp(- 1e14 * time_array * time_array) + 0.3  # gauss
# waveform = np.exp(- 1. * time_array / 1e-7) * np.sin(2. * np.pi * 1e8 * time_array) + 12.8  # FID


wp = WaveformProcessing(
    sampling_rate,
    waveform,
    dtype="c8",
    logger=get_normal_logger(logger_name=__name__, level="info"),
)  # WaveformProcessingのインスタンスの作成
wp.zoom(
    low_time=-1e-7, high_time=1e-7, low_frequency=0.5e8, high_frequency=1.5e8
)  # -1e-7 sから+1e-7 sの波形を拡大, 50 MHzから150 MHzのスペクトルを拡大
wp.save(
    file_name="before", time_metric_prefix="u", frequency_metric_prefix="G"
)  # グラフの保存
wp.show(time_metric_prefix="u", frequency_metric_prefix="G")  # グラフの保存
###########################################################################################################
# wp.dc_block()  # DC成分を0にする.
# wp.zerofill(20000)  # 20000ポイントゼロフィルする.
wp.bpf(0.5e8, 1.5e8)  # 50-150 MHzのFIRフィルタでフィルタリングする.
# wp.fir_bpf(0.9e8, 1.1e8, 100)  # 100の大きさの90-110 MHzのFIRフィルタでフィルタリングする.
# wp.hanning()  # ハニング窓をかける.
###########################################################################################################
wp.zoom(
    low_time=-1e-7, high_time=1e-7, low_frequency=0.5e8, high_frequency=1.5e8
)  # -1e-7 sから+1e-7 sの波形を拡大, 50 MHzから150 MHzのスペクトルを拡大
wp.save(
    file_name="after", time_metric_prefix="u", frequency_metric_prefix="G"
)  # グラフの保存
wp.show(time_metric_prefix="u", frequency_metric_prefix="G")  # グラフの保存
del wp  # WaveformProcessingのインスタンスの削除
