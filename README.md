# 汎用パッケージ
utilityは汎用のモジュールが格納されたパッケージです. / This is a package that stores general-purpose modules.  

## 動作確認について / Operation check
8つのバッチファイル(sample___init__.bat, sample_constants.bat, sample_file_manager.bat, sample_graph_utility.bat, sample_logging_utility.bat, sample_others.bat, sample_read_write.bat, sample_waveform_processing)をそれぞれ実行し, エラーメッセージが表示されないことを確認してください.  

## アップデート方法について / About update
update.batを実行することによってGitLabのレポジトリから最新のプログラムをダウンロードしてきてAnacondaのライブラリにインストールすることができます.  

## 更新予定 / Update schedule
[graph_utility.py] カラーマップのプログラムの改良  
[read_write.py] 日本語を使った時の文字化けの修正  

## 更新履歴 / Change log
ver 1.11.0 - [read_write.py] 引数の初期値を設定  
ver 1.10.2 - [constants.py] EXTENSION_DICTの「.ipynb」のフォルダー名を変更  
ver 1.10.1 - コメントの追加，修正  
ver 1.10.0 - [file_manager.py] arrange_file_nameの修正  
ver 1.9.0 - [file_manager.py] クラスJsonFileManagerを追加  
ver 1.8.0 - [waveform_processing.py] コンストラクタに引数"fourier_transform"を追加  
ver 1.7.0 - [file_manager.py] arrange_folder及びdelete_same_filesの高速化，[read_write.py] read_pickle及びwrite_pickleの追加  
ver 1.6.1 - [file_manager.py] delete_same_filesの修正  
ver 1.6.0 - バックスラッシュをスラッシュに変更  
ver 1.5.0 - [constants.py] 単位の追加  
ver 1.4.0 - [constants.py] EXTENSION_DICTの値の変更  
ver 1.3.0 - [waveform_processing.py] メソッドshowの追加  
ver 1.2.3 - [waveform_processing.py] waveform_processingの修正  
ver 1.2.2 - [file_manager.py] delete_same_filesの修正  
ver 1.2.1 - [read_write.py] write_jsonのバグの修正  
ver 1.2.0 - [others.py] 有効数字に関する関数significant_digitsを追加  
ver 1.1.0 - [others.py] select_file，select_folderの実行時，最前面に出力されるように調整  
ver 1.0.0 - [others.py] get_program_name_and_file_extensionの削除，get_program_nameを追加  
ver 0.4.0 - [others.py] select_fileの引数"extension"の追加  
ver 0.3.1 - [read_write.py] 微修正  
ver 0.3.0 - [waveform_processing.py] WaveformProcessing.saveの引数file_pathのデフォルト値の変更  
ver 0.2.0 - [\_\_init\_\_.py] 定数"PROGRAM_NAME"の追加  
ver 0.1.0 - [read_write.py] 各関数に引数"addition"を追加(追加でファイルに書き込むか選択できる)  
ver 0.0.0 - 最初のバージョン  
