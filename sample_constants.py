from utility.constants import PI, PLANCK_CONSTANT, LOGGING_LEVEL_DICT, METRIC_PREFIX


print(PI)  # 円周率
print(PLANCK_CONSTANT)  # プランク定数
print(LOGGING_LEVEL_DICT)  # ログレベル
print(METRIC_PREFIX)  # SI接頭辞
