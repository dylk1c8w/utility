from setuptools import setup


setup(
    name="utility",
    version="1.11.0",
    author="Yuta Kawai",
    author_email="pygo3xmdy11u@gmail.com",
    packages=["utility",],
    package_data={"utility": ["fonts/ipaexg.ttf", "fonts/ipaexm.ttf",],},
    include_package_data=True,
    install_requires=[],
)
