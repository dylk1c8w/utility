import numpy as np
from utility import CURRENT_PATH
from utility.read_write import (
    read_pickle,
    write_binary_file,
    read_binary_file,
    write_pickle,
    write_text,
    read_text,
    write_csv,
    read_csv,
    write_json,
    read_json,
)


# sine wave
x_array = np.linspace(0.0, 1.0, 50, endpoint=False)
y_array = np.sin(2.0 * np.pi * 10.0 * x_array)


write_binary_file(
    binary_data=y_array,
    binary_path=CURRENT_PATH + "/dat",
    binary_name="read_write",
    addition=True,
    format_character="f",
)  # バイナリファイルの作成
print(
    read_binary_file(
        binary_path="{0}/read_write".format(CURRENT_PATH + "/dat"), format_character="f"
    )
)  # バイナリファイルの読み込み
write_text(
    ["abc", "あいうえお", "xyz",],
    text_path=CURRENT_PATH + "/dat",
    text_name="read_write",
    addition=True,
)  # テキストファイルの作成
print(read_text("{0}/read_write.txt".format(CURRENT_PATH + "/dat")))  # テキストファイルの読み込み
write_csv(
    (x_array, y_array),
    header=["x_array", "y_array"],
    csv_path=CURRENT_PATH + "/dat",
    csv_name="read_write",
    addition=True,
)  # CSVファイルの作成
print(
    read_csv(csv_path="{0}/read_write.csv".format(CURRENT_PATH + "/dat"), header=(1, 0))
)  # CSVファイルの読み込み
write_json(
    json_data={"1": 1, 2: (2, 3), "3": "4", "4": 3.14},
    json_path=CURRENT_PATH + "/dat",
    json_name="read_write",
    addition=True,
)  # JSONファイルの作成
print(
    read_json(json_path="{0}/read_write.json".format(CURRENT_PATH + "/dat"))
)  # JSONファイルの読み込み
write_pickle(
    pickle_data=x_array, pickle_path=CURRENT_PATH + "/dat", pickle_name="read_write"
)  # PICKLEファイルの作成
print(read_pickle(pickle_path="{0}/read_write.pickle".format(CURRENT_PATH + "/dat")))  # PICKLEファイルの読み込み
