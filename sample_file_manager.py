from utility.file_manager import (
    get_file_folder_path_list,
    get_all_file_folder_path_list,
    get_folder_path_list,
    get_all_folder_path_list,
    get_file_path_list,
    get_all_file_path_list,
    get_new_file_name,
)
from utility.logging_utility import *
from utility.others import *


path = select_folder()  # 操作したいフォルダーを選択
logger = get_normal_logger()  # ロガーを準備
print(get_file_folder_path_list(path, logger=logger))  # ファイルとフォルダーのパスのリストを取得
print(
    get_all_file_folder_path_list(path, logger=logger)
)  # フォルダーの中身も含めたファイルとフォルダーのパスのリストを取得
print(get_folder_path_list(path, logger=logger))  # フォルダーのパスのリストを取得
print(get_all_folder_path_list(path, logger=logger))  # フォルダーの中身も含めたフォルダーのパスのリストを取得
print(get_file_path_list(path, logger=logger))  # ファイルのパスのリストを取得
print(get_all_file_path_list(path, logger=logger))  # フォルダーの中身も含めたファイルのパスのリストを取得
# move_file(select_file(), select_folder(), overwrite=True, logger=logger)  # ファイルを移動する
# copy_file(select_file(), select_folder(), logger=logger)  # ファイルをコピーする
# move_folder(select_folder(), select_folder(), logger=logger)  # フォルダーを移動する
# copy_folder(select_folder(), select_folder(), logger=logger)  # フォルダーをコピーする
# arrange_folder(select_folder(), excepted_extension=[".ini", ".inf",], logger=logger)  # フォルダの中身を整理する
# delete_all_empty_folder(select_folder(), logger=logger)  # 空のフォルダーをすべて削除する
# delete_same_files(select_folder(), logger=logger)  # フォルダ内の同じデータのファイルを削除する
# arrange_file_name(select_folder(), logger=logger)  # フォルダ内のファイル名を調整する
print(get_new_file_name(select_file()))  # 新しいファイル名を取得する
