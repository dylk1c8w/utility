from utility.logging_utility import (
    get_normal_logger,
    get_file_output_logger,
    kill_loggers,
)


my_logger = get_normal_logger(
    logger_name=__name__,
    format_string="["
    "%(name)s|"
    "%(levelno)s|"
    "%(levelname)s|"
    "%(pathname)s|"
    "%(filename)s|"
    "%(module)s|"
    "%(lineno)d|"
    "%(funcName)s|"
    "%(created)f|"
    "%(asctime)s|"
    "%(msecs)d|"
    "%(relativeCreated)d|"
    "%(thread)d|"
    "%(threadName)s|"
    "%(process)d|"
    "%(message)s]",
    level="debug",
)  # ロガーの作成
my_logger.debug("debug")  # "debug"と表示
del my_logger  # 変数my_loggerの削除
my_logger1 = get_normal_logger(logger_name="my_logger1", level="debug")  # ロガーの作成
my_logger2 = get_file_output_logger(
    logger_name="my_logger2", log_file_name="my_logger2", level="info"
)  # ファイルに記録するロガーの作成
my_logger3 = get_normal_logger(logger_name="my_logger3", level="warning")  # ロガーの作成
my_logger4 = get_file_output_logger(
    logger_name="my_logger4", log_file_name="my_logger4", level="error"
)  # ファイルに記録するロガーの作成
my_logger1.debug("debug")
my_logger2.debug("debug")  # 表示されない
my_logger3.debug("debug")  # 表示されない
my_logger4.debug("debug")  # 表示されない
my_logger1.info("info")
my_logger2.info("info")
my_logger3.info("info")  # 表示されない
my_logger4.info("info")  # 表示されない
my_logger1.warning("warning")
my_logger2.warning("warning")
my_logger3.warning("warning")
my_logger4.warning("warning")  # 表示されない
my_logger1.error("error")
my_logger2.error("error")
my_logger3.error("error")
my_logger4.error("error")
my_logger1.critical("critical")
my_logger2.critical("critical")
my_logger3.critical("critical")
my_logger4.critical("critical")
del my_logger1  # 変数my_logger1の削除
del my_logger2  # 変数my_logger2の削除
del my_logger3  # 変数my_logger3の削除
del my_logger4  # 変数my_logger4の削除
kill_loggers()  # loggerを全て消去
