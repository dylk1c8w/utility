import numpy as np
from utility.graph_utility import GraphUtility


SAMPLING_RATE = 1e9  # サンプリングレート(1 GSPS)
TIME_LENGTH = 1e-5  # 時間の長さ(10 us)


time_array = np.linspace(
    -TIME_LENGTH / 2.0,
    TIME_LENGTH / 2.0,
    int(round(SAMPLING_RATE * TIME_LENGTH)),
    endpoint=False,
)  # 時間軸の配列
waveform_array = np.sin(2.0 * np.pi * 1e8 * time_array) * np.exp(
    -1e13 * time_array * time_array
)  # 波形の配列


# 違うグラフを3回続けて表示する
for i in range(2):
    graph = GraphUtility(
        font_size=15,
        figure_size=(8, 8),
        x_axis_min=-500,
        x_axis_max=500,
        y_axis_min=-0.75,
        y_axis_max=0.75,
    )  # GraphUtilityのインスタンスの作成
    graph.plot(
        1e9 * time_array, float(i + 1) * waveform_array
    )  # waveform_arrayにfloat(i + 1)をかけたものをプロット
    graph.line(100.0, "v", line_width=3.0, linestyle=":", color="r")  # 縦方向に線を引く
    graph.line(0.5, "h", line_width=3.0, linestyle=":", color="b")  # 横方向に線を引く
    graph.save(
        figure_name="GraphUtility{}".format(i),
        x_axis_label="時間 (ns)",
        y_axis_label="電圧 (V)",
    )  # グラフを保存する
    graph.show()  # グラフを表示する
    del graph  # インスタンスを削除する


# 2系列を同じグラフに表示する
sin_wave = np.sin(2.0 * np.pi * 1e6 * time_array)  # sin波
cos_wave = np.cos(2.0 * np.pi * 1e6 * time_array)  # cos波
graph = GraphUtility()  # GraphUtilityのインスタンスの作成
graph.single_plot(sin_wave, label="sin", color="r")  # sin_waveをプロット
graph.single_plot(cos_wave, label="cos", color="b")  # cos_waveをプロット
graph.save(
    figure_name="sin_cos", x_axis_label="横軸", y_axis_label="縦軸", legend=True
)  # グラフを保存する
graph.show(x_axis_label="横軸", y_axis_label="縦軸", legend=True)  # グラフを表示する
del graph  # インスタンスを削除する


# カラーマップを作成する
gamma = 10.0
v_vertical = 1.0
t_array = np.linspace(-1.0, 1.0, 1001)
v_parallel_array = np.linspace(-1.0, 1.0, 1001)
t_array, v_parallel_array = np.meshgrid(t_array, v_parallel_array)  # 配列の要素から格子列
p_array = (
    1.0
    - np.square(
        np.cos(
            gamma
            * t_array
            * np.sqrt(np.square(v_parallel_array) + np.square(v_vertical))
        )
    )
    - (
        np.square(v_parallel_array)
        / (np.square(v_parallel_array) + np.square(v_vertical))
    )
    * np.square(
        np.sin(
            gamma
            * t_array
            * np.sqrt(np.square(v_parallel_array) + np.square(v_vertical))
        )
    )
)
graph = GraphUtility()  # GraphUtilityのインスタンスの作成
graph.make_color_map(
    p_array, x_axis_min=-1.0, x_axis_max=1.0, y_axis_min=-1.0, y_axis_max=1.0
)  # カラーマップを作成
graph.show()  # グラフを表示する
del graph  # インスタンスを削除する
