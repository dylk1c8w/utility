from utility import (
    COMMAND_LINE_ARGUMENTS,
    CURRENT_PATH,
    MAIN_FILE_DIRECTORY_PATH,
    PROGRAM_NAME,
)


print(COMMAND_LINE_ARGUMENTS)  # コマンドライン引数を出力する
print(CURRENT_PATH)  # カレントディレクトリを出力する
print(MAIN_FILE_DIRECTORY_PATH)  # メインのファイル(実行するPythonファイル)が保存されているパスを出力する
print(PROGRAM_NAME)  # 実行するPythonファイル名を出力する
