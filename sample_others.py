from utility import CURRENT_PATH
from utility.others import (
    beep,
    transpose_list,
    make_str_int,
    get_time,
    get_variable_name,
    check_variable,
    get_directory_path,
    get_program_name,
    select_file,
    select_folder,
    get_yes_or_no,
    series,
    significant_digits,
    sort_based_on_a_list,
    generate_a_random_string,
)


def func1(i):
    return 3 * i + 2


def func2(*i):
    return 6 * i[0] - 7 * i[1] * i[1]


beep()  # ビープ音を鳴らす.
print(transpose_list([[3, 4], [1,],]))  # 2次元配列を転置する.
print(make_str_int(134, 4))  # 小数または整数を任意の桁数で丸め, 文字列で返す.
print(get_time())  # 時刻を取得し, 文字列に直す.
x = 80
print(get_variable_name(x))  # 変数名を取得する.
y = "Hello, world."
check_variable(y)  # 変数名を表示する.
print(get_directory_path(CURRENT_PATH))  # 引数のパスを絶対パスにし, 親ディレクトリのパスを返す.
print(get_program_name(CURRENT_PATH))  # 引数のパスのファイル名を取得する.
print(select_file(extension=[".py", ".bat"]))  # 処理ファイルをGUIから選択し, その絶対パスを返す.
print(select_folder())  # 処理フォルダーをGUIから選択し, その絶対パスを返す.
print(get_yes_or_no("Yes or No?(y/n)"))  # YesかNoが入力するまで質問を繰り返す.
print(series(func1, N=10, k=2))  # Σ(i=2, 10)(3i+2)を計算する.
print(series(func2, N=(16, 6), k=(3, -3)))  # Σ(i=3, 16)(j=-3, 6)(6i -7j^2)を計算する.
print(significant_digits(3.4234521532e-14, 5))  # 有効数字5桁で返す
x = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l"]
y = [6, 0, 4, 8, 1, 9, 3, 0, 5, 1, 7, 2]
print(sort_based_on_a_list(sorted_list=x, standard_list=y))  # yをソートする順序でxを並び変える
print(generate_a_random_string(16))  # ランダムで長さ16の文字列を作成する
